#!/usr/bin/env bash
#
# Execute with:
# bash <(curl -sSL https://gitlab.com/alexpooley/gdk-installer/raw/master/install.sh)


# Install GDK on to a development system.
# It should be possible to run this script via curl to install or update a GDK
# installation.

# Print GitLab logo to terminal.
tanuki()
{
  # Converted using the excellent https://dom111.github.io/image-to-ansi/
  # Replaced \e[48;5;0m with \e[0m so it's just terminal background. Probably not as nice on light backgrounds.
  printf '\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[0m  \e[0m  \e[0m
\e[0m  \e[48;5;208m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;208m  \e[0m  \e[0m
\e[0m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[0m  \e[0m
\e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m
\e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m
\e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m
\e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m
\e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m
\e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m
\e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m
\e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;202m  \e[48;5;166m  \e[48;5;166m  \e[48;5;202m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;208m  \e[48;5;166m  \e[48;5;166m  \e[48;5;208m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;214m  \e[48;5;214m  \e[48;5;202m  \e[48;5;202m  \e[48;5;214m  \e[48;5;214m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[48;5;208m  \e[48;5;208m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
\e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m  \e[0m
';
}

gl_printf()
{
  # Tanuki dark orange
  printf "\033[38;5;214m$1\033[0m"
}

has_command()
{
  hash $1 2>/dev/null
}

gdk_install_homebrew()
{
  if has_command brew; then gl_printf "Skipping homebrew install\n"
  else
    gl_printf "Installing homebrew\n"
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi
}

gdk_install_rbenv()
{
  if has_command rbenv; then gl_printf "Skipping rbenv install\n"
  else
    gl_printf "Installing rbenv\n"
    brew install rbenv
    rbenv init
  fi
}

# Install required GitLab ruby version with rbenv.
# Assumes rbenv already installed.
# Sets global variables `ruby_version`
gdk_install_ruby()
{
  gl_printf "GitLab requires ruby version... "
  ruby_version=`curl -s https://gitlab.com/gitlab-org/gitlab/raw/master/.ruby-version`
  gl_printf "$ruby_version\n"

  # Install and use required ruby version.
  versions=`rbenv versions`
  if [[ $versions == *$ruby_version* ]]; then
    gl_printf "Skipping ruby $ruby_version install\n"
  else
    rbenv install $ruby_version
  fi
  rbenv local $ruby_version

  # Ensure bundler installed.
  if has_command bundle; then
    gem install bundler &> /dev/null
  fi
}

# After GDK and dependencies have been installed, instantiate an instance of
# the GDK on the system. A copy+paste from
# https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/set-up-gdk.md
gdk_setup()
{
  gl_printf "Setting up GDK\n"
  gem install gitlab-development-kit &> /dev/null

  local default_install_dir="$HOME/gdk"
  gl_printf "GDK installation directory [$default_install_dir]: "
  read install_dir
  install_dir=${install_dir:-$default_install_dir}
  # TODO Handle when dir exists. Is it GDK or something else?
  gl_printf "Initializing GDK in $install_dir\n"
  gdk init $install_dir &> /dev/null
  cd $install_dir

  # For some reason rbenv local is reset to system.
  rbenv local $ruby_version
  gl_printf "Installing GDK. This can take a while...\n"
  gdk install &> /dev/null
}

# @param variable name E.g. "PATH"
# @param value E.g. "/home/alex/bin"
gdk_install_prepend_in_profile()
{
  echo "export $1=\"$2:\$$1\"" >> $profile
}

gdk_install_osx_brew()
{
  brew install git redis postgresql@10 libiconv pkg-config cmake go openssl coreutils re2 graphicsmagick node@12 gpg runit icu4c exiftool
  brew install yarn --ignore-dependencies
  brew link pkg-config
  brew pin node@12 icu4c readline
  bundle config build.eventmachine --with-cppflags=-I/usr/local/opt/openssl/include

  gdk_install_prepend_in_profile "PATH" "/usr/local/opt/postgresql@10/bin:/usr/local/opt/node@12/bin"
  gdk_install_prepend_in_profile "PKG_CONFIG_PATH" "/usr/local/opt/icu4c/lib/pkgconfig"
  source $profile

  brew cask install google-chrome chromedriver
}

# TODO Call from gdk_install_osx
gdk_install_osx_macports()
{
  sudo port install git redis libiconv postgresql10-server icu pkgconfig cmake nodejs12 go openssl npm5 yarn coreutils re2 GraphicsMagick runit exiftool
  bundle config build.eventmachine --with-cppflags=-I/opt/local/include/openssl

  gdk_install_prepend_in_profile "PATH", "/opt/local/lib/postgresql10/bin/"
  source $profile
}

# GDK install on OSX.
gdk_install_osx()
{
  # Default OSX to homebrew + rbenv, and ignore the macports path.
  gdk_install_homebrew
  gdk_install_rbenv
  gdk_install_ruby
  gdk_setup
}

# Setup script variables.
gdk_install_setup()
{
  # Define a `profile` variable with the shell profile path.
  case $(basename $SHELL) in
    "bash") profile="$HOME/.bash_profile" ;;
    # "csh")  profile="~/.cshrc" ;;
    # "fish") profile="~/.config/fish/config.fish" ;;
    # "ksh")  profile="~/.kshrc" ;;
    # "mksh") profile="~/.kshrc" ;;
    # "sh")   profile="~/.bash_profile" ;;
    # "tcsh") profile="~/.tcshrc" ;;
    "zsh")  profile="$HOME/.zshrc" ;;
  esac
}

# Entry point to install GDK on any OS.
gdk_install()
{
  gdk_install_setup

  # Branch install based on operating system.
  case "$OSTYPE" in
    # OSX
    darwin*)  gdk_install_osx ;;
    #linux*)   echo "LINUX" ;;
    #bsd*)     echo "BSD" ;;
    # msys*)    echo "WINDOWS" ;;
    *)        gl_printf "Unhandled OS ${OSTYPE}. Try https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md" ;;
  esac

  gl_printf "Done\n"
}


tanuki
gdk_install
